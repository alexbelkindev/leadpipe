/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.7.17 : Database - leadpipe
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`leadpipe` /*!40100 DEFAULT CHARACTER SET latin1 */;

/*Table structure for table `links` */

DROP TABLE IF EXISTS `links`;

CREATE TABLE `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) DEFAULT NULL,
  `status` varchar(12) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `link` (`link`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `links` */

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `optionName` varchar(32) NOT NULL,
  `optionValue` varchar(255) NOT NULL,
  PRIMARY KEY (`optionName`),
  UNIQUE KEY `optionName` (`optionName`,`optionValue`),
  UNIQUE KEY `optionName_2` (`optionName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `options` */

insert  into `options`(`optionName`,`optionValue`) values 
('title','Product Hunter');

/*Table structure for table `product_categories` */

DROP TABLE IF EXISTS `product_categories`;

CREATE TABLE `product_categories` (
  `pid` varchar(11) DEFAULT NULL,
  `category_name` varchar(128) DEFAULT NULL,
  UNIQUE KEY `pid` (`pid`,`category_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_categories` */

/*Table structure for table `product_links` */

DROP TABLE IF EXISTS `product_links`;

CREATE TABLE `product_links` (
  `pid` varchar(11) DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_links` */

/*Table structure for table `product_medals` */

DROP TABLE IF EXISTS `product_medals`;

CREATE TABLE `product_medals` (
  `pid` varchar(11) DEFAULT NULL,
  `title` text,
  `date` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_medals` */

/*Table structure for table `product_mobile_app_links` */

DROP TABLE IF EXISTS `product_mobile_app_links`;

CREATE TABLE `product_mobile_app_links` (
  `pid` varchar(11) DEFAULT NULL,
  `link` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_mobile_app_links` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `pid` varchar(11) NOT NULL,
  `productName` text,
  `productUrl` text,
  `productDescription` longtext,
  `productVotes` varchar(11) DEFAULT NULL,
  `productDate` int(11) DEFAULT NULL,
  `productRating` float DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `products` */

/*Table structure for table `products_hunting_status` */

DROP TABLE IF EXISTS `products_hunting_status`;

CREATE TABLE `products_hunting_status` (
  `pid` varchar(11) DEFAULT NULL,
  `link` text,
  `status` varchar(12) DEFAULT NULL,
  `error_message` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `products_hunting_status` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
