/**
 * Created by alexbelkin on 28.10.2017.
 */
var loadingImg = document.createElement('img');
loadingImg.src = 'images/loading.gif';

function refreshPage() {
    location.reload();
}

function grabLinks() {
    var statusString = $('div.grabLinksResults');
    statusString.html(loadingImg);
    $.ajax({
        url: 'index.php?action=grabLinks',
        success: function (data) {
            var result = jQuery.parseJSON(data);
            statusString.html(result.message);
        }
    });
}

function parseProducts() {
    var statusString = $('div.parseProductsResults');
    var parsedProducts = 0;
    sendParseRequest();
    function sendParseRequest() {
        statusString.html("Products parsed count: " + parsedProducts + '');
        $.ajax({
            url: 'index.php?action=parseProducts',
            success: function (data) {
                var jsonData = jQuery.parseJSON(data);
                if (jsonData.result == 'true') {
                    parsedProducts++;
                    sendParseRequest();
                } else {
                    statusString.text("Parse process finished and parsed " + parsedProducts + ' products.');
                }
            }
        });
    }
}

