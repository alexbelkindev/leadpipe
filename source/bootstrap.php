<?php

define('ROOT_DIR', dirname(__DIR__));

#init composer psr-4 autoloader
require_once ROOT_DIR . '/vendor/autoload.php';

use Utils\Route;

Route::start();