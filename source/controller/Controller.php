<?php

namespace Controller;

use Model\View;
use Model\MainModel;

class Controller
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->model = new MainModel();
        $this->view = new View();
    }

    protected function view($viewName, $data)
    {
        $this->view = new View($viewName, $data);
    }

    protected function model($modelName)
    {
        return new $modelName();
    }

    public function indexAction()
    {
    }
}