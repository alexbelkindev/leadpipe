<?php

namespace Controller;

use Model\Grabber;
use Model\Parser;
use Model\Dumper;

class MainController extends Controller
{
    public function indexAction()
    {
        parent::indexAction();
        $data = $this->model->getData();
        $content = $this->view->render('Index', $data['info']);
        $html = $this->view->render('Main', [
            'title' => $data['title'],
            'content' => $content,
        ]);
        echo $html;
    }

    public function grabLinksAction()
    {
        $model = new Grabber();
        $result = $model->startGrabLinks();
        echo $result;
    }

    public function parseProductsAction()
    {
        $model = new Parser();
        $result = $model->startParseProducts();
        echo $result;
    }

    public function getDumpAction()
    {
        $model = new Dumper();
        $data = $model->getProductsDump();

    }
}