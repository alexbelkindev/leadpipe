<?php

namespace Model;

use Utils\Database;

const BLOCK_SIZE = 500;
const DELIMITER = ',';
const ENCLOSURE = '"';

class Dumper
{
    protected $publicDir = ROOT_DIR . '/public/';
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstance()->getConnection();
    }

    public function getProductsDump()
    {
        $out = fopen('php://memory', 'w');
        fputcsv($out, array_keys($this->getContainer()), DELIMITER, ENCLOSURE);
        $this->writeDataFromDatabase($out);
        fseek($out, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="dump.csv";');
        fpassthru($out);
    }

    protected function getContainer()
    {
        return [
            'link' => '',
            'name' => '',
            'description' => '',
            'url' => '',
            'votes' => '',
            'rating' => '',
            'date' => '',
            'badgeTitle' => '',
            'badgeDate' => '',
            'tags' => '',
            'angelList' => '',
            'facebook' => '',
            'instagram' => '',
            'github' => '',
            'medium' => '',
            'twitter' => '',
            'iTunes' => '',
            'googlePlay' => '',
            'logo' => '',
            'screen' => '',
        ];
    }

    protected function writeDataFromDatabase($out)
    {
        $count = $this->getProductsCount();
        $blocks = $this->getBlocksCount($count);

        for ($i = 0; $i <= $blocks; $i++) {
            $offset = $i * BLOCK_SIZE;
            $data = $this->createContainers($offset);
            $this->getBasicProductInfo($data);
            $this->attachProductLinks($data);
            $this->attachBadges($data);
            $this->attachTags($data);
            $this->attachSocialLinks($data);
            $this->attachWebAppLinks($data);
            foreach ($data as $product) {
                fputcsv($out, $product, DELIMITER, ENCLOSURE);
            }
        }
    }

    protected function attachWebAppLinks(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , `link`
        , `type`
        FROM
        `product_mobile_app_links`
        WHERE
        pid IN (%s);
        ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $links = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($links as $link) {
            $data[$link['id']][$link['type']] = $link['link'];
        }
    }

    protected function attachSocialLinks(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , `link`
        , `type`
        FROM
        `product_links`
        WHERE
        pid IN (%s);
        ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $links = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($links as $link) {
            $data[$link['id']][lcfirst($link['type'])] = $link['link'];
        }
    }

    protected function attachTags(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , concat('[', `category_name`, ']') AS `tag`
        FROM
        `product_categories`
        WHERE
        pid IN (%s);
        ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $tags = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($tags as $tag) {
            $data[$tag['id']]['tags'] .= $tag['tag'];
        }
    }

    protected function attachBadges(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , `title` AS `badgeTitle`
        , `date` as `badgeDate`
        FROM
        `product_medals`
        WHERE
        pid IN (%s);
        ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $badges = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($badges as $badge) {
            $data[$badge['id']]['badgeTitle'] = trim($badge['badgeTitle']);
            $data[$badge['id']]['badgeDate'] = trim($badge['badgeDate']);
        }
    }

    protected function attachProductLinks(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , `link` AS `link`
        FROM
        `products_hunting_status`
        WHERE
        pid IN (%s);
        ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $links = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($links as $link) {
            $data[$link['id']]['link'] = $link['link'];
        }
    }

    protected function getBasicProductInfo(&$data)
    {
        $selectStmt = sprintf("
        SELECT
        `pid` AS `id`
        , `productName` AS `name`
        , `productUrl` AS `url`
        , `productDescription` AS `description`
        , `productVotes` AS `votes`
        , FROM_UNIXTIME(`productDate`) AS `date`
        , `productRating` AS `rating`
        , `productLogo` AS `logo`
        , `productScreen` AS `screen`
        FROM
        `products`
        WHERE
        pid IN (%s);
          ", implode(',', array_keys($data)));

        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $results = $select->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($results as $result) {
            $id = $result['id'];
            unset($result['id']);
            foreach ($result as $key => $value) {
                if ($key == 'description') {
                    $value = trim(preg_replace('/\s+/', ' ', $value));
                }
                $data[$id][$key] = $value;
            }
        }
    }

    protected function createContainers($offset)
    {
        $selectStmt = sprintf("SELECT pid FROM products LIMIT %d, %d;",
            $offset, BLOCK_SIZE);
        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $list = array_flip($select->fetchAll(\PDO::FETCH_COLUMN));
        foreach ($list as $key => &$value) {
            $value = $this->getContainer();
        }

        return $list;
    }

    protected function getBlocksCount($count)
    {
        $blocks = round($count / BLOCK_SIZE) - 1;

        return $blocks;
    }

    protected function getProductsCount()
    {
        $selectStmt = 'SELECT COUNT(pid) FROM products;';
        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $data = $select->fetchColumn();

        return $data;
    }
}