<?php
/**
 * Created by PhpStorm.
 * User: alexbelkin
 * Date: 24.10.2017
 * Time: 20:33
 */

namespace Model;

use Utils\Database;
use Utils\Curl;

class Grabber
{
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstance()->getConnection();
    }

    public function startGrabLinks()
    {
        $success = [];
        for ($sitemap = 1; $sitemap < 5; $sitemap++) {
            $entities = $this->getLinksFromSitemap($sitemap);
            if ($entities === false) {
                break;
            }
            $entities = $this->filterEntities($entities);
            $parsedLinks = $this->processEntities($entities);
            $success = array_merge($success, $parsedLinks);
            sleep(2);
        }
        return json_encode([
            'message' => sprintf('Last sitemap reached. Parsed %d links', count($success))
        ]);
    }

    protected function filterEntities($entities)
    {
        $filtered = [];
        $count = 0;
        foreach ($entities as $url) {
            if ($this->filterUrl($url)) {
                continue;
            }
            if ($this->duplicateCheck($url)) {
                continue;
            }
            $filtered[] = $url;
            $count++;
        }
        return $filtered;
    }

    protected function processEntities($entities)
    {
        $productLinks = [];
        foreach ($entities as $url) {
            if ($this->insertEntity($url)) {
                $productLinks[] = $url;
            }
        }
        return $productLinks;
    }

    protected function insertEntity($url)
    {
        $insertStmt = sprintf("INSERT INTO links (link, status) VALUES ('%s', '%s')",
            addslashes($url), 'waiting');
        $insert = $this->database->prepare($insertStmt);
        $insert->execute();

        if ($insert->rowCount() !== 1) {
            return false;
        }

        return true;
    }

    protected function filterUrl($url)
    {
        $urlMask = "@^http[s]?:\/\/www.producthunt.com\/posts\/[^\/]*$@i";

        if (preg_match($urlMask, $url) !== 1) {
            return true;
        }

        return false;

    }

    protected function duplicateCheck($url)
    {
        $selectStmt = sprintf("SELECT COUNT(id) FROM links WHERE link = '%s';", addslashes($url));
        $preparedStmt = $this->database->prepare($selectStmt);
        $preparedStmt->execute();

        if ((int)$preparedStmt->fetchColumn() > 0) {
            return true;
        }

        return false;
    }

    public function getLinksFromSitemap($sitemap)
    {
        $productLinks = [];
        $url = sprintf('https://producthunt.s3.amazonaws.com/sitemaps/sitemap%d.xml.gz', $sitemap);
        $data = new Curl($url);
        $data->createCurl();
        $page = $data->getWebPage();
        if (strpos($page, '<?xml version="1.0" encoding="UTF-8"?>') !== false) {
            return false;
        }
        $page = gzdecode($page);
        $xmlData = simplexml_load_string($page);
        foreach ($xmlData as $item) {
            $productLinks[] = (string)$item->loc[0];
        }
        return $productLinks;
    }
}