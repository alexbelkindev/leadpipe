<?php

namespace Model;

use Utils\Database;

class MainModel
{
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstance()->getConnection();
    }

    public function getData()
    {
        $data['title'] = $this->getTitle();
        $data['info'] = $this->getInfo();

        return $data;
    }

    protected function getTitle()
    {
        $stmt = $this->database->prepare('SELECT optionValue FROM options WHERE optionName = "title";');
        $stmt->execute();
        $title = $stmt->fetchColumn();

        return $title;
    }

    public function getInfo()
    {
        $stmt['totalLinks'] = "SELECT COUNT(id) FROM links";
        $stmt['totalParsed'] = $stmt['totalLinks'] . "WHERE status = 'done'";
        $stmt['totalProducts'] = "SELECT COUNT(pid)FROM products_hunting_status";
        $stmt['totalProductsParsed'] = $stmt['totalProducts'] . " WHERE status = 'done'";
        $stmt['totalProductsError'] = $stmt['totalProducts'] . " WHERE status = 'error'";

        $result = [];

        foreach ($stmt as $query) {
            $selectStmt = $this->database->prepare($query);
            $selectStmt->execute();
            $result[] = $selectStmt->fetchAll();
        }

        return [
            'total_links' => isset($result[0][0][0]) ? (int)$result[0][0][0] : 0,
            'total_parsed' => isset($result[1][0][0]) ? (int)$result[1][0][0] : 0,
            'total_products' => isset($result[2][0][0]) ? (int)$result[2][0][0] : 0,
            'total_products_parsed' => isset($result[3][0][0]) ? (int)$result[3][0][0] : 0,
            'total_products_errors' => isset($result[4][0][0]) ? (int)$result[4][0][0] : 0,
        ];
    }

}