<?php

namespace Model;

use Utils\Database;
use Utils\Curl;
use Sunra\PhpSimple\HtmlDomParser;

const PRODUCT_ID = 'productID';
const PRODUCT_NAME = 'productName';
const PRODUCT_URL = 'productUrl';
CONST PRODUCT_DESCRIPTION = 'productDescription';
const PRODUCT_VOTES = 'productVotes';
const PRODUCT_CATEGORIES = 'productCategories';
const PRODUCT_SOCIAL_LINKS = 'productSocialLinks';
const PRODUCT_MEDALS = 'productMedals';
const PRODUCT_MOBILE_APP_LINKS = 'productMobileAppLinks';
const PRODUCT_DATE = 'productDate';
const PRODUCT_RATING = 'productRating';
const PRODUCT_LOGO = 'productLogo';
const PRODUCT_SCREEN = 'productScreen';

class Parser
{

    protected $errorMsg;
    protected $database;

    public function __construct()
    {
        $this->database = Database::getInstance()->getConnection();
    }

    public function startParseProducts()
    {
        $link = $this->getLinkToParse();
        if (!$link) {
            return json_encode([
                'result' => 'false',
            ]);
        }
        $html = $this->getProductPageCode($link);
        $container = $this->parseProductInfo($html);
        if (!is_array($container)) {
            $this->errorMsg = 'Empty container.';
        } else {
            $container = $this->attachProductID($container, $link);
            $container = $this->normalizeContainer($container);
            $this->saveProductContainer($container);
            $this->writeStatusInfo($container, $link);
        }
        $this->updateLinkStatus($link);
        sleep(3);
        return json_encode([
            'result' => 'true',
        ]);
    }

    protected function writeStatusInfo($container, $link)
    {
        if ($this->productStatusExist($container[PRODUCT_ID])) {
            if (!empty($this->errorMsg)) {
                $stmt = sprintf("UPDATE products_hunting_status SET status = 'error', error_message = '%s'
                                 WHERE pid = '%s';", $this->errorMsg, $container[PRODUCT_ID]);
            } else {
                $stmt = sprintf("UPDATE products_hunting_status SET status = 'done' WHERE pid = '%s'",
                    $container[PRODUCT_ID]);
            }
        } else {
            if (!empty($this->errorMsg)) {
                $stmt = sprintf("INSERT INTO products_hunting_status VALUES ('%s', '%s', 'error', '%s')",
                    $container[PRODUCT_ID], $link, $this->errorMsg);
            } else {
                $stmt = sprintf("INSERT INTO products_hunting_status (pid, link, status) VALUES ('%s', '%s', 'done')",
                    $container[PRODUCT_ID], $link);
            }
        }

        $write = $this->database->prepare($stmt);

        $write->execute();
    }

    protected function productStatusExist($id)
    {
        $selectStmt = sprintf("SELECT COUNT(pid) FROM product_hunting_status WHERE pid = '%s';", $id);
        $select = $this->database->prepare($selectStmt);
        $select->execute();
        $count = $select->fetchColumn();
        if (!empty($count)) {
            return true;
        }

        return false;
    }

    protected function attachProductID($container, $link)
    {
        $container[PRODUCT_ID] = crc32($link);

        return $container;
    }

    protected function saveProductContainer($container)
    {
        $this->insertBasicProductInfo($container);
        if (!empty($container[PRODUCT_CATEGORIES])) {
            $this->insertArrayInfo('product_categories', $container[PRODUCT_ID], $container[PRODUCT_CATEGORIES]);
        }
        if (!empty($container[PRODUCT_SOCIAL_LINKS])) {
            $this->insertLinkInfo('product_links', $container[PRODUCT_ID], $container[PRODUCT_SOCIAL_LINKS]);

        }
        if (!empty($container[PRODUCT_MEDALS])) {
            $this->insertMedalInfo('product_medals', $container[PRODUCT_ID], $container[PRODUCT_MEDALS]);

        }
        if (!empty($container[PRODUCT_MOBILE_APP_LINKS])) {
            $this->insertLinkInfo('product_mobile_app_links', $container[PRODUCT_ID], $container[PRODUCT_MOBILE_APP_LINKS]);
        }
    }

    protected function insertLinkInfo($table, $id, $array)
    {
        foreach ($array as $value) {
            if (empty($value['url'])) {
                continue;
            }
            $insertStmt = sprintf("INSERT INTO %s VALUES ('%s', '%s', '%s');",
                $table, $id, $value['url'], $value['type']);
            $insert = $this->database->prepare($insertStmt);
            $insert->execute();
        }

    }

    protected function insertMedalInfo($table, $id, $value)
    {
        if (empty($value['title']) || empty($value['date'])) {
            return;
        }
        $insertStmt = sprintf("INSERT INTO %s VALUES ('%s', '%s', '%s');",
            $table, $id, $value['title'], $value['date']);
        $insert = $this->database->prepare($insertStmt);
        $insert->execute();
    }

    protected function insertArrayInfo($table, $id, $values)
    {
        foreach ($values as $value) {
            $insertStmt = sprintf("INSERT INTO %s VALUES ('%s', '%s');",
                $table, $id, $value);
            $insert = $this->database->prepare($insertStmt);
            $insert->execute();
        }
    }

    protected function normalizeContainer(&$container)
    {
        foreach ($container as $value) {
            $this->filterItem($value);
        }

        return $container;
    }

    protected function filterItem(&$value)
    {
        if (is_array($value)) {
            foreach ($value as $item) {
                if (is_array($item)) {
                    $this->filterItem($item);
                    continue;
                }
                trim(addslashes($item));
            }
            return;
        }
        trim(addslashes($value));
    }

    protected function insertBasicProductInfo($container)
    {
        $insertStmt = "INSERT IGNORE products VALUES (:id, :name, :url, :desc, :votes, :date, :rating, :logo, :screen);";
        $insert = $this->database->prepare($insertStmt);
        $insert->bindParam(':id', $container[PRODUCT_ID]);
        $insert->bindParam(':name', $container[PRODUCT_NAME]);
        $insert->bindParam(':url', $container[PRODUCT_URL]);
        $insert->bindParam(':desc', $container[PRODUCT_DESCRIPTION]);
        $insert->bindParam(':votes', $container[PRODUCT_VOTES]);
        $insert->bindParam(':date', $container[PRODUCT_DATE]);
        $insert->bindParam(':rating', $container[PRODUCT_RATING]);
        $insert->bindParam(':logo', $container[PRODUCT_LOGO]);
        $insert->bindParam(':screen', $container[PRODUCT_SCREEN]);
        $insert->execute();
    }

    protected function parseProductInfo($html)
    {
        foreach ($this->getParseMethods() as $method) {
            $parsedProductInfo = $this->$method($html);
            if (!empty($parsedProductInfo)) {
                return $parsedProductInfo;
            }
        }
        return false;
    }

    protected function updateLinkStatus($link)
    {
        if (!empty($this->errorMsg)) {
            $updateStmt = sprintf("UPDATE links SET status = 'error', message = '%s' WHERE link = '%s';",
                $this->errorMsg, $link);
        } else {
            $updateStmt = sprintf("UPDATE links SET status = 'done' WHERE link = '%s';", $link);
        }
        $update = $this->database->prepare($updateStmt);
        $update->execute();
    }

    protected function getProductInfoContainer()
    {
        return [
            PRODUCT_ID => '',
            PRODUCT_NAME => '',
            PRODUCT_URL => '',
            PRODUCT_DESCRIPTION => '',
            PRODUCT_VOTES => 0,
            PRODUCT_CATEGORIES => [],
            PRODUCT_SOCIAL_LINKS => [],
            PRODUCT_MEDALS => [],
            PRODUCT_MOBILE_APP_LINKS => [],
            PRODUCT_DATE => null,
            PRODUCT_RATING => null,
            PRODUCT_LOGO => '',
            PRODUCT_SCREEN => '',
        ];
    }

    public function getProductPageCode($link)
    {
        $handler = new Curl($link);
        $handler->createCurl();
        $html = $handler->getWebPage();
        return $html;
    }

    protected function parseProductInfoWithHtmlDOM($html)
    {
        $container = $this->getProductInfoContainer();
        $document = HtmlDomParser::str_get_html($html);
        if (!$document) {
            $this->errorMsg = 'Broken document';
            return $container;
        }
        $container = $this->getDataFromJson($document, $container);
        $container = $this->getDataFromSelectors($document, $container);
        $container = $this->getProductUrl($document, $container);
        $container[PRODUCT_SOCIAL_LINKS] = $this->getProductSocialLinks($document);
        $container[PRODUCT_MOBILE_APP_LINKS] = $this->getProductMobileAppLinks($document);
        $container[PRODUCT_MEDALS] = $this->getProductMedals($document);
        $container[PRODUCT_DATE] = $this->getProductDate($document);
        if (empty($container[PRODUCT_CATEGORIES])) {
            $data[PRODUCT_CATEGORIES] = $this->getProductCategories($document);
        }

        return $container;
    }

    protected function getProductUrl($document, $container)
    {
        $element = $document->find('a.link_9bebc');
        if (!isset($element[0])) {
            return $container;
        }
        $linkNodes = $element[0]->find('div.name_a05ce');
        foreach ($linkNodes as $node) {
            if ($node->text() == 'Website') {
                $url = $element[0]->title;
                $container[PRODUCT_URL] = $url;
            }
        }
        return $container;
    }

    protected function getProductDate($document)
    {
        $tag = 'div.timestamp_38f9d time';
        $element = $document->find($tag);
        if (empty($element)) {
            return null;
        }
        $date = strtotime($element[0]->datetime);
        return $date;
    }

    protected function getProductSocialLinks($document)
    {
        $socialLinks = [];
        foreach ($document->find('.list_fc099 a') as $element) {
            $socialLinks[] = [
                'url' => $element->href,
                'type' => $element->title,
            ];
        }

        return $socialLinks;
    }

    protected function getProductMedals($document)
    {
        $medalInfo = [
            'title' => '',
            'date' => '',
        ];
        $selector = 'div.side_51c8d';
        $element = $document->find($selector);
        if (empty($element)) {
            return [];
        }
        $medalBlock = $element[0]->find('span');
        $medalInfo['title'] = $medalBlock[0]->text();
        $medalInfo['date'] = $medalBlock[1]->text();

        return $medalInfo;
    }

    protected function getProductMobileAppLinks($document)
    {
        $links = [];
        $selector = 'a.link_9bebc';
        $iTunes = '@itunes\.apple\.com@i';
        $googlePlay = '@play\.google\.com@i';
        foreach ($document->find($selector) as $element) {
            if (preg_match($iTunes, $element->title) > 0) {
                $links[] = [
                    'url' => (string)$element->title,
                    'type' => 'iTunes',
                ];
            }
            if (preg_match($googlePlay, $element->title) > 0) {
                $links[] = [
                    'url' => (string)$element->title,
                    'type' => 'googlePlay',
                ];
            }
        }
        return $links;
    }

    protected function getDataFromJson($document, $productInfo)
    {
        $jsonElement = $document->find('script[type="application/ld+json"]');
        if (!isset($jsonElement[0])) {
            return [];
        }
        $json = trim((string)$jsonElement[0]->innerText());
        $data = json_decode($json, true);
        if (!$data) {
            return $productInfo;
        }
        $productInfo[PRODUCT_NAME] = !empty($data['name']) ? $data['name'] : '';
        $productInfo[PRODUCT_DESCRIPTION] = !empty($data['description']) ? $data['description'] : '';
        $productInfo[PRODUCT_CATEGORIES] = !empty($data['applicationCategory']) ? $data['applicationCategory'] : [];
        $productInfo[PRODUCT_RATING] = !empty($data['aggregateRating']['ratingValue']) ?
            round($data['aggregateRating']['ratingValue'], 2) : null;
        $productInfo[PRODUCT_LOGO] = !empty($data['image']) ? $data['image'] : '';
        $productInfo[PRODUCT_SCREEN] = isset($data['screenshot'][0]) ? $data['screenshot'][0] : '';

        return $productInfo;
    }

    protected function getProductCategories($document)
    {
        $productCategories = [];
        $selector = 'div.headerInfo_66903 div a';
        foreach ($document->find($selector) as $element) {
            $productCategories[] = trim((string)$element->text());
        }

        return $productCategories;
    }

    protected function getDataFromSelectors($document, $productInfo)
    {
        foreach ($this->getSelectors() as $key => $xPath) {
            if (empty($productInfo[$key])) {
                $elem = $document->find($xPath);
                if (isset($elem[0])) {
                    $productInfo[$key] = trim((string)$elem[0]->text());
                }
            }
        }
        return $productInfo;
    }

    protected function getSelectors()
    {
        return [
            PRODUCT_NAME => 'h1.headerPostName_e7ab4 a',
            PRODUCT_DESCRIPTION => 'h2.headerPostTagline_98494',
            PRODUCT_VOTES => 'span.bigButtonCount_10448',
        ];
    }

    protected function parseProductInfoWithRegExp($html)
    {
        return [];
    }

    protected function parseProductInfoWithPhantomJS($html)
    {
        //TODO: add phantom JS method
        return [];
    }

    protected function getParseMethods()
    {
        return [
            'parseProductInfoWithHtmlDOM',
            'parseProductInfoWithRegExp',
            'parseProductInfoWithPhantomJS',
        ];
    }

    public function getLinkToParse()
    {
        $selectStmt = "SELECT link FROM links WHERE status = 'waiting' LIMIT 1;";
        $preparedStmt = $this->database->prepare($selectStmt);
        $preparedStmt->execute();
        $link = $preparedStmt->fetchColumn();
        if (empty($link)) {
            return false;
        }
        $updateStmt = sprintf("UPDATE links SET status = 'running' WHERE link = '%s'", addslashes($link));
        $preparedUpdateStmt = $this->database->prepare($updateStmt);
        $preparedUpdateStmt->execute();

        return $link;
    }
}