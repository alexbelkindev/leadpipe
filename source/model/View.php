<?php

namespace Model;

class View
{
    public function getHtml()
    {
        $html = $this->render();

        return $html;
    }

    public function render($file = 'Main', $options = [])
    {
        $template = ROOT_DIR . '/source/view/' . $file . 'View.tpl';
        $html = '';

        try {
            if (!is_file($template)) {
                throw new \Exception('Template file ' . $template . ' not found.');
            }

            if(filesize($template) === 0) {
                throw new \Exception('Template file ' . $template . ' is empty.');
            }

            $html =  file_get_contents($template);

            $this->renderVariables($html, $options);

        } catch (\Exception $e) {
            $html = "Error: #" . $e->getMessage() . PHP_EOL;
        } finally {
            return $html;
        }
    }

    protected function renderVariables(&$html, &$options)
    {
        foreach ($options as $key => $option) {
            if ($option !== null || $option !== false) {

                $key = '{{' . strtoupper($key) . '}}';

                $html = str_replace($key, $option, $html);
            }
        }
    }
}