<?php
/**
 * Created by PhpStorm.
 * User: alexbelkin
 * Date: 24.10.2017
 * Time: 20:38
 */

namespace Utils;

class Curl
{
    protected $userAgent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
    protected $cookieFileLocation = ROOT_DIR . '/source/utils/coockie.txt';
    protected $referer = 'https://www.google.com/';
    protected $webPage;
    protected $status;
    protected $timeout = 30;

    protected $url;
    protected $followLocation;
    protected $maxRedirects;
    protected $noBody;
    protected $includeHeader;
    protected $binaryTransfer;


    public function __construct($url, $followLocation = true, $timeOut = 30, $maxRedirects = 4,
                                $binaryTransfer = false, $includeHeader = false, $noBody = false)
    {
        $this->url = $url;
        $this->followlocation = $followLocation;
        $this->maxRedirects = $maxRedirects;
        $this->noBody = $noBody;
        $this->includeHeader = $includeHeader;
        $this->binaryTransfer = $binaryTransfer;
    }

    public function setReferer($referer)
    {
        $this->referer = $referer;
    }

    public function setCookieFileLocation($cookieFileLocation)
    {
        $this->cookieFileLocation = $cookieFileLocation;
    }

    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }

    public function createCurl($url = '')
    {
        if (!empty($url)) {
            $this->url = $url;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, $this->maxRedirects);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->followlocation);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookieFileLocation);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookieFileLocation);
        curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($ch, CURLOPT_HEADER, $this->includeHeader ? true : false);
        curl_setopt($ch, CURLOPT_NOBODY, $this->noBody ? true : false);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, $this->binaryTransfer ? true : false);

        /* If you are lonely enough to talk with cURL, while drinking a cup of wine...
        curl_setopt($ch, CURLOPT_VERBOSE, true); */

        $this->webPage = curl_exec($ch);
        $this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
    }

    public function getHttpStatus()
    {
        return $this->status;
    }

    public function getWebPage(){
        return $this->webPage;
    }

}