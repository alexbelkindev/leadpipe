<?php

namespace Utils;

class Database
{
    private static $_instance;
    private $_connection;
    private $host = 'localhost';
    private $user = 'root';
    private $pass = 'root';
    private $database = 'leadpipe';

    private function __construct()
    {
        try {
            $this->_connection = new \PDO(sprintf('mysql:host=%s;dbname=%s', $this->host, $this->database),
                $this->user, $this->pass);
        } catch (\Exception $e) {
            echo "Error: #", $e->getCode() . PHP_EOL . $e->getMessage(), PHP_EOL;
        }
    }

    private final function __clone()
    {
    }

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getConnection()
    {
        return $this->_connection;
    }
}