<?php

namespace Utils;

class Route
{
    static $controller;
    static $action;

    static function start()
    {
        self::getRoutes();
        $controller = 'Controller\\' . ucfirst(self::$controller) . 'Controller';
        $action = self::$action . 'Action';

        if (class_exists($controller)) {
            $pageController = new $controller();
        } else {
            throw new \Exception('Controller for this page doesn`t exist.');
        }

        if (method_exists($pageController, $action)) {
            $pageController->$action();
        }
    }

    static function getRoutes()
    {
        self::$controller = isset($_GET['page']) ? trim($_GET['page']) : 'main';
        self::$action = isset($_GET['action']) ? trim($_GET['action']) : 'index';
    }

}