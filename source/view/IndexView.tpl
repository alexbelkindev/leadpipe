<h1>Product hunter</h1>
<h2>Statistics</h2>
<ul class="stat_list">
    <li><span class="label">Total links: </span><span class="data">{{TOTAL_LINKS}}</span></li>
    <li><span class="label">Parsed links: </span><span class="data">{{TOTAL_PARSED}}</span></li>
    <li><span class="label">Founded products: </span><span class="data">{{TOTAL_PRODUCTS_PARSED}}</span></li>
    <li><span class="label">Products with errors: </span><span class="data">{{TOTAL_PRODUCTS_ERRORS}}</span></li>
</ul>
<h2>Options</h2>
<div class="option"><a onclick="grabLinks()" href="#">Parse links from the sitemaps</a></div>
<div class="grabLinksResults"></div>
<div class="option"><a onclick="parseProducts()" href="#">Start product parse process</a></div>
<div class="parseProductsResults"></div>
<!--<div class="option"><a onclick="parseErrorProducts()" href="#">Restart error products</a></div>
<div class="option"><a onclick="openErrorLog()" href="#">Open error log</a></div>-->
<div class="option"><a href="index.php?action=getDump">Download CSV dump</a></div>
<div class="option"><a onclick="refreshPage()" href="#">Refresh statistics</a></div>
